# hod-mock

This repository contains a [cosmoSIS](https://bitbucket.org/joezuntz/cosmosis/) module to compute 2-pt correlation functions using light-cone mock catalogs produced with Zheng05 HOD model ([arXiv:astro-ph/0408564](https://arxiv.org/abs/astro-ph/0408564)), wrapping the implementations of HOD mock generation from [halotools](https://halotools.readthedocs.io/en/latest/) and 2pt function calculation from [treecorr](https://rmjarvis.github.io/TreeCorr/_build/html/overview.html).
