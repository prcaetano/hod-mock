from cosmosis.gaussian_likelihood import SingleValueGaussianLikelihood


class NgalLikelihood(SingleValueGaussianLikelihood):
    section = "hod_ngal"
    name = "gal_volume_density"
    like_name = "hod"

    def build_data(self):
        gal_density = self.options.block["sample_parameters", "density_of_galaxies"]
        gal_density_err = self.options.block["sample_parameters", "density_of_galaxies_error"]
        self.mean = gal_density
        self.sigma = gal_density_err
        return self.mean, self.sigma


setup, execute, cleanup = NgalLikelihood.build_module()

