import numpy as np
import george
from cosmosis.datablock import names as section_names
from cosmosis.runtime.declare import declare_module
from cosmosis.runtime.emulated_module import EmulatedModule
from cosmosis.runtime.emulator import GPPCALogEmulator, GPLogEmulator, GPPCAEmulator, GPEmulator
from george.kernels import ExpSquaredKernel, Matern32Kernel, EmptyKernel, PolynomialKernel


class EmulatedHODMock(EmulatedModule):
    x_section = section_names.data_vector
    y_section = section_names.data_vector
    x_name = "hod_x"
    y_name = "hod_y"
    yerr_name = "hod_y_err"
    ycov_name = "hod_cov"

    measured_gal_density_section = "sample_parameters"
    measured_gal_density_name = "density_of_galaxies"
    measured_gal_density_err_name = "density_of_galaxies_error"

    def __init__(self,my_config,my_name):
        self.block = my_config

        ntries = my_config.get_int(my_name, "ntrainingtries", 12)
        npca = my_config.get_int(my_name, "npca", 5)
        n_varied_pars = my_config.get_int(my_name, "nparameters", 6)
        fit_white_noise = my_config.get_bool(my_name, "fit_white_noise", True)
        fit_mean = my_config.get_bool(my_name, "fit_mean", False)
        kernel_spec_string = my_config.get_string(my_name, "kernel", "exp_squared + matern32")
        self.include_gal_density = my_config.get_bool(my_name, "include_gal_density", True)

        self.acf_file = my_config.get_string("sample_files", "acf", "")

        do_pca = my_config.get_bool(my_name, "do_pca", True)
        do_log = my_config.get_bool(my_name, "do_log", True)
        weight_pca = my_config.get_bool(my_name, "weight_pca", False)
        x_as_index = my_config.get_bool(my_name, "x_as_index", True)
        if weight_pca:
            pca_implementation = "wpca"
        else:
            pca_implementation = "pca"

        # Building emulation class
        kernel_parcels = kernel_spec_string.replace(" ", "").lower().split("+")
        n_varied_pars += int(not x_as_index)
        kernel = EmptyKernel(ndim=n_varied_pars, axes=np.arange(n_varied_pars))
        npars = int(fit_white_noise) + int(fit_mean)
        if "exp_squared" in kernel_parcels:
            npars += n_varied_pars + 1
            kernel += 1.0 * ExpSquaredKernel(np.ones(n_varied_pars),
                                             ndim=n_varied_pars,
                                             axes=np.arange(n_varied_pars))
        if "matern32" in kernel_parcels:
            npars += n_varied_pars + 1
            kernel += 0.1 * Matern32Kernel(0.1*np.ones(n_varied_pars),
                                           ndim=n_varied_pars,
                                           axes=np.arange(n_varied_pars))
        if "linear" in kernel_parcels:
            npars += 1
            k = 10.0 * PolynomialKernel(order=1, ndim=n_varied_pars,
                                        axes=np.arange(n_varied_pars), log_sigma2=0.)
            for par in k.get_parameter_names():
                if "log_sigma2" in par:
                    k.freeze_parameter(par)
            kernel += k

        a = 1e-6
        b = 1e6
        par_range = [[a, b] for _ in range(npars)]
        par_range = np.log(par_range)

        solver = george.BasicSolver
        white_noise = 1e0
        mean_model = 0.
        method = "L-BFGS-B"

        if do_log:
            if do_pca:
                self.emulator_generator = GPPCALogEmulator.build_emulator(kernel,
                                                                          par_range=par_range,
                                                                          ntries=ntries,
                                                                          white_noise=white_noise,
                                                                          fit_white_noise=fit_white_noise,
                                                                          mean=mean_model,
                                                                          fit_mean=fit_mean,
                                                                          solver=solver,
                                                                          method=method,
                                                                          npca=npca,
                                                                          logx=False,
                                                                          logy=True,
                                                                          pca_implementation=pca_implementation,
                                                                         )
            else:
                self.emulator_generator = GPLogEmulator.build_emulator(kernel,
                                                                       par_range=par_range,
                                                                       ntries=ntries,
                                                                       white_noise=white_noise,
                                                                       fit_white_noise=fit_white_noise,
                                                                       mean=mean_model,
                                                                       fit_mean=fit_mean,
                                                                       solver=solver,
                                                                       method=method,
                                                                       logx=False,
                                                                       logy=True,
                                                                       x_as_index=x_as_index,
                                                                      )
        else:
            if do_pca:
                self.emulator_generator = GPPCAEmulator.build_emulator(kernel,
                                                                       par_range=par_range,
                                                                       ntries=ntries,
                                                                       white_noise=white_noise,
                                                                       fit_white_noise=fit_white_noise,
                                                                       mean=mean_model,
                                                                       fit_mean=fit_mean,
                                                                       solver=solver,
                                                                       method=method,
                                                                       npca=npca,
                                                                       logx=False,
                                                                       logy=True,
                                                                       pca_implementation=pca_implementation,
                                                                      )
            else:
                self.emulator_generator = GPLogEmulator.build_emulator(kernel,
                                                                       par_range=par_range,
                                                                       ntries=ntries,
                                                                       white_noise=white_noise,
                                                                       fit_white_noise=fit_white_noise,
                                                                       mean=mean_model,
                                                                       fit_mean=fit_mean,
                                                                       solver=solver,
                                                                       method=method,
                                                                       x_as_index=x_as_index,
                                                                      )


        super(EmulatedHODMock, self).__init__(my_config, my_name)


    def build_data(self):
        self.data_x, self.data_y, self.data_y_err = np.loadtxt(self.acf_file, unpack=True)
        if self.include_gal_density:
            if self.block.has_value(self.measured_gal_density_section, self.measured_gal_density_name):
                gal_density = self.block[self.measured_gal_density_section, self.measured_gal_density_name]
                gal_density_err = self.block[self.measured_gal_density_section,
                                             self.measured_gal_density_err_name]

                new_data_x = np.zeros(len(self.data_x) + 1)
                new_data_y = gal_density * np.ones(len(self.data_x) + 1)
                new_data_y_err = gal_density_err * np.ones(len(self.data_x) + 1)
                new_data_x[1:] = self.data_x
                new_data_y[1:] = self.data_y
                new_data_y_err[1:] = self.data_y_err

                self.data_x = new_data_x
                self.data_y = new_data_y
                self.data_y_err = new_data_y_err
            else:
                raise RuntimeError("You should specify the measured galaxy density")
        return self.data_x, self.data_y

declare_module(EmulatedHODMock)

