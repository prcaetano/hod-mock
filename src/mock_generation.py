import os
import sys
import numpy as np
import healpy as hp
import pandas as pd
import warnings
from halotools.sim_manager import UserSuppliedHaloCatalog
from astropy.cosmology import FlatLambdaCDM, z_at_value
from astropy.constants import c
from astropy import units as u
from astropy.io import fits
from cosmosis.datablock import BlockError
from halotools.empirical_models import PrebuiltHodModelFactory, get_halo_mass_key
from halotools.empirical_models import halo_mass_to_halo_radius
from halotools.sim_manager import CachedHaloCatalog
from halotools.custom_exceptions import HalotoolsError
from scipy.interpolate import interp1d

#from memory_profiler import profile

#DEBUG = False
DEBUG = True

sim_parameters = "simulation_parameters"
hod_parameters = "hod_parameters"
module_section = "hod_mock"

alpha_small = 0.1
alpha_smaller = 0.01


def estimate_ngals(hodpars, halo_masses, mdef, conc_mass_model):
    """
    Estimates the number of galaxies to be generated when populating the halo catalog.

    Parameters:
        hodpars: dict
            describes the given hod parameters
        halo_masses: np.ndarray
            masses, in solar masses, of halos
        mdef: str
            halo definition to use (e.g., "vir" for virial and "200c" for 200 times the critical density)
        conc_mass_model: str
            concentration model of NFW profile. Can be "direct_from_halo_catalog" (default) or "dutton_maccio14"

    Returns:
        ngals

        ngals: int
    """
    temp_model = PrebuiltHodModelFactory('zheng07', modulate_with_cenocc=True,
                                         conc_mass_model=conc_mass_model, mdef=mdef)

    temp_model.param_dict['alpha'] = hodpars['alpha']
    temp_model.param_dict['logM0'] = hodpars['mass0']
    temp_model.param_dict['logM1'] = hodpars['mass1']
    temp_model.param_dict['logMmin'] = hodpars['massmin']
    temp_model.param_dict['sigma_logM'] = hodpars['sigmam']

    central_occupations = temp_model.mean_occupation_centrals(prim_haloprop=halo_masses)
    central_occupations *= hodpars["fcentral"]
    satellites_occupations = temp_model.mean_occupation_satellites(prim_haloprop=halo_masses)
    totals_occupations = central_occupations + satellites_occupations
    ngals = int(totals_occupations.sum())

    return ngals


def populate_halos(hodpars, halocat, cosmology, model=None,
                   mdef="vir", conc_mass_model="direct_from_halo_catalog",
                   num_ptcl_requirement=300):
    """
    Creates synthetic galaxy sample by populating halos of a simulation.

    Parameters:
        hodpars: dict
            describes hod parameters. Should have the keys sigmam, massmin, alpha, mass0 and mass1, fcentral
        halocat: halotools UserSuppliedHaloCatalog object
            halo catalog object
        cosmology: astropy.cosmology.core.FlatLambdaCDM
            astropy cosmology object
        model: halotools.empirical_models.HodModelFactory
            if supplied, reuses halotools model from previous computation to save time
        mdef: str
            halo definition to use (e.g., "vir" for virial and "200c" for 200 times the critical density)
        conc_mass_model: str
            concentration model of NFW profile. Can be "direct_from_halo_catalog" (default) or "dutton_maccio14"
        num_ptcl_requirement: int
            minimum number of particles a halo should have to be considered

    Returns:
        gals, model.mock

        gals: array

        model:
            model object
    """
    mass_key = get_halo_mass_key(mdef)

    with warnings.catch_warnings():
        warnings.filterwarnings('ignore', r"Using a non-tuple")

        if model is None:
            model = PrebuiltHodModelFactory('zheng07', modulate_with_cenocc=True,
                                            conc_mass_model=conc_mass_model, mdef=mdef)
            model.populate_mock(halocat, halo_mass_column_key=mass_key,
                                Num_ptcl_requirement=num_ptcl_requirement)

        model.param_dict['alpha'] = hodpars['alpha']
        model.param_dict['logM0'] = hodpars['mass0']
        model.param_dict['logM1'] = hodpars['mass1']
        model.param_dict['logMmin'] = hodpars['massmin']
        model.param_dict['sigma_logM'] = hodpars['sigmam']
        fcentral = hodpars['fcentral']

        model.mock.populate()
        gals = model.mock.galaxy_table
        gals.keep_columns(["x", "y", "z", "vx", "vy", "vz", "gal_type"])

    idx_centrals, = np.nonzero(gals['gal_type'] == 'centrals')
    idx_satellites, = np.nonzero(gals['gal_type'] == 'satellites')
    n_centrals_selected = min(int(fcentral * len(idx_centrals)), len(idx_centrals))
    try:
        idx_centrals_selected = np.random.choice(idx_centrals, n_centrals_selected, replace=False)
    except ValueError:
        idx_centrals_selected = np.array([])
    idx = np.append(idx_centrals_selected, idx_satellites)
    idx_drop = np.setdiff1d(np.arange(len(gals)), idx)
    gals.remove_rows(idx_drop)
    gals_selected = gals

    gal_pos = np.array(gals_selected["x","y","z"].to_pandas())
    gal_vel = np.array(gals_selected["vx", "vy", "vz"].to_pandas())

    comoving_distances = np.linalg.norm(gal_pos, axis=1)
    los_velocity = (gal_vel*gal_pos).sum(axis=1) / comoving_distances

    c_km_s = c.to('km/s').value
    h = cosmology.H0.to(u.km / u.Mpc / u.s).value / 100

    zgrid = np.linspace(0., 2., 200000)
    comoving_distances_grid = cosmology.comoving_distance(zgrid).value * h
    z_cos = np.interp(comoving_distances, comoving_distances_grid, zgrid)

    redshift = z_cos + los_velocity / c_km_s * (1. + z_cos)
    mask = redshift > 0
    gal_pos = gal_pos[mask]
    redshift = redshift[mask]
    comoving_distances = comoving_distances[mask]

    theta = np.arccos(gal_pos[:, 2]/comoving_distances)
    phi = np.arctan2(gal_pos[:, 1], gal_pos[:, 0])

    ra = phi * 180. / np.pi
    dec = (np.pi/2. - theta) * 180. / np.pi

    return np.c_[ra, dec, redshift], model


def add_photoz_uncertainties(gals_z, z_meas, std_meas, bias_meas):
    """
    Adds photoz bias and scatter, interpolated from measured ones, to the simulated gals_z redshifts.

    Parameters:
        gals_z: list-like
            simulated redshift values
        z_meas: list-like
            "true" (that is, spectroscopic) redshifts where dispersion and bias was measured
        std_meas: list-like
            dispersion, measured as the standard deviation, at z_meas redshifts
        bias_meas: list-like
            bias, measured as median of photoz - specz, at z_meas redshifts

    Returns:
        gals_photoz: list-like
            perturbed redshift values
    """
    interp_std = interp1d(z_meas, std_meas)
    interp_bias = interp1d(z_meas, bias_meas)

    std = interp_std(gals_z)
    bias = interp_bias(gals_z)
    deviates = std * np.random.normal(size=len(gals_z)) + bias

    gals_photoz = deviates + gals_z

    return gals_photoz


def eqinbin(gals, zmin, zmax, returnmask=False):
    """
    Return galaxies in redshift slice between zmin and zmax.

    Parameters:
        gals: np.ndarray, shape (N, 3)
            array holding ra, dec and photoz of galaxies as columns
        zmin: float
            left extreme of bin
        zmax: float
            right extreme of bin

        returnmask(False): bool
            whether to return mask of selected rows or not

    Returns:
        if not returnmask:
            binned_gals
        else:
            binned_gals, mask

        binned_gals: np.ndarray, shape (Nselected, 3)
            array holding ra, dec and photoz of galaxies on given bin
        mask: np.ndarray, shape (Nselected)
            bool array of selected galaxies
    """
    gals_redshift = gals[:,2]
    mask = (gals_redshift >= zmin) & (gals_redshift < zmax)
    binned_gals = gals[mask]

    if returnmask:
        return binned_gals, mask
    else:
        return binned_gals


def load_hod_dict(block):
    """ Loads hod parameters as dictionary from datablock. """
    hod_keys = ["sigmam", "massmin", "alpha", "mass0", "mass1", "fcentral"]
    hodpars = {key: block[hod_parameters, key] for key in hod_keys}
    return hodpars


def setup(options):
    block = options
    data = dict()

    # Check if mpi is using to prevent race conditions later on
    try:
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()
        if size > 1:
            has_mpi_enabled = True
        else:
            has_mpi_enabled = False
    except ImportError:
        comm = None
        rank = 0
        size = 1
        has_mpi_enabled = False

    # Loading simulation configurations
    redshift = block.get_double(sim_parameters, "redshift")
    Lbox = block.get_double(sim_parameters, "Lbox")
    particle_mass = block.get_double(sim_parameters, "particle_mass")
    omegam = block.get_double(sim_parameters, "omegam")
    omegab = block.get_double(sim_parameters, "omegab")
    h = block.get_double(sim_parameters, "h")
    ns = block.get_double(sim_parameters, "ns")
    omegal = block.get_double(sim_parameters, "omegal")
    sigma8 = block.get_double(sim_parameters, "sigma8")
    w = block.get_double(sim_parameters, "w")
    catalog_fname = block.get_string(sim_parameters, "halo_catalog_fname")
    mdef = block.get_string(sim_parameters, "mdef")
    sim_name = block.get_string(sim_parameters, "sim_name")
    halo_finder = block.get_string(sim_parameters, "halo_finder")
    conc_mass_model = block.get_string(sim_parameters, "conc_mass_model")
    version_name = block.get_string(sim_parameters, "version_name")
    num_ptcl_requirement = block.get_int(sim_parameters, "halo_min_num_particles")
    estimated_area = block.get_double(sim_parameters, "area")

    # Loading module configurations
    halo_catalogs_dir = options.get(module_section, "halo_catalogs_dir")
    nside_halo_subcatalogs = options.get(module_section, "nside_halo_subcatalogs")
    nrealizations = options.get(module_section, "nrealizations")
    zmin = options.get(module_section, "zmin")
    zmax = options.get(module_section, "zmax")
    try:
        max_memory = options.get(module_section, "max_memory")
    except BlockError:
        max_memory = np.inf

    # Loading simulation data
    cosmology = FlatLambdaCDM(Om0=omegam, Ob0=omegab, H0=100*h, Tcmb0=2.73)
    hdul = fits.open(catalog_fname, memmap=True)

    logm = hdul[1].data["lmhalo"]
    z = hdul[1].data["z_cgal"]

    max_z = z_at_value(lambda x: cosmology.comoving_distance(x).value * h, Lbox)
    mask_max_z = z < max_z
    mask_min_mass = logm > np.log10(num_ptcl_requirement * particle_mass)
    mask_halos = mask_max_z & mask_min_mass

    xhalo = hdul[1].data["xhalo"]
    yhalo = hdul[1].data["yhalo"]
    mask_halos = mask_halos & (xhalo > 0.) & (yhalo > 0.)

    xhalo = hdul[1].data["xhalo"][mask_halos]
    yhalo = hdul[1].data["yhalo"][mask_halos]
    zhalo = hdul[1].data["zhalo"][mask_halos]

    logm = logm[mask_halos]
    m = 10**logm
    nhalos = len(logm)

    comoving_distances = np.linalg.norm(np.c_[xhalo, yhalo, zhalo], axis=1)
    theta = np.arccos(zhalo / comoving_distances)
    phi = np.arctan2(yhalo, xhalo)
    ra = phi * 180. / np.pi
    dec = (np.pi / 2. - theta) * 180. / np.pi
    hpix = hp.ang2pix(nside_halo_subcatalogs, ra, dec, lonlat=True)
    occupied_pixels = np.unique(hpix)

    vxhalo = hdul[1].data["vxhalo"][mask_halos]
    vyhalo = hdul[1].data["vyhalo"][mask_halos]
    vzhalo = hdul[1].data["vzhalo"][mask_halos]

    z = hdul[1].data["z_cgal"][mask_halos]

    rvir = halo_mass_to_halo_radius(m, cosmology, z, mdef=mdef)

    ids = np.arange(nhalos)

    fraction_on_zbin = ((z > zmin) & (z < zmax)).astype(float).sum() / len(z)

    # Slicing halo catalog
    halocat_slice_fname_template = halo_catalogs_dir + sim_name + version_name + "_npix_{}_{:04d}_{}.hdf5"
    nslices = len(np.unique(hpix))

    if DEBUG and rank==0:
        print("Halo catalog contains {} halos.".format(nhalos))
        print("nslices = {}, hpix of halo subcatalogs = {}".format(nslices, nside_halo_subcatalogs))

    # Checking if sliced halo catalogs exist
    nonexistent_slices = []
    for pixel in occupied_pixels:
        halocat_slice_fname = halocat_slice_fname_template.format(nside_halo_subcatalogs, pixel, "std")
        if not os.path.exists(halocat_slice_fname):
            nonexistent_slices.append(pixel)

    # Syncronizes everyone to prevent race conditions while master creates/load sliced catalogs
    if has_mpi_enabled:
        comm.barrier()

    # Create non existent sliced halo catalogs, (only master)
    if rank==0:
        if DEBUG:
            print("--------------")
            print("Creating non existent sliced catalogs.")
        for pixel in nonexistent_slices:
            if DEBUG:
                print("MPI Rank: {}, MPI Size: {}, Pixel: {}".format(rank, size, pixel))
            halocat_slice_fname = halocat_slice_fname_template.format(nside_halo_subcatalogs, pixel, "std")
            mask_selection = pixel == hpix
            halo_catalog = UserSuppliedHaloCatalog(redshift=redshift, Lbox=Lbox, particle_mass=particle_mass,
                                                   halo_x=xhalo[mask_selection],
                                                   halo_y=yhalo[mask_selection],
                                                   halo_z=zhalo[mask_selection],
                                                   halo_vx=vxhalo[mask_selection],
                                                   halo_vy=vyhalo[mask_selection],
                                                   halo_vz=vzhalo[mask_selection],
                                                   halo_id=ids[mask_selection],
                                                   halo_upid=-1*np.ones_like(ids)[mask_selection],
                                                   halo_hostid=ids[mask_selection],
                                                   halo_mvir=m[mask_selection],
                                                   halo_rvir=rvir[mask_selection])
            halo_catalog.add_halocat_to_cache(fname=halocat_slice_fname,
                                              simname=sim_name,
                                              halo_finder=halo_finder,
                                              version_name=version_name + "_npix_{}_{:04d}".format(nside_halo_subcatalogs, pixel),
                                              processing_notes="nside_halo_subcatalogs = {}".format(nside_halo_subcatalogs),
                                              overwrite=True)
            del halo_catalog

    ## Create small and smaller versions of halo catalog (1/10 and 1/100 of area, resp.)
    if rank==0:
        for alpha, name in [(alpha_small, "small"), (alpha_smaller, "smaller")]:
            delta_dec = 45. - 1./2 * np.arcsin(1 - alpha) * 180. / np.pi      ## Cuts 1./ alpha of area around
            delta_ra = np.sqrt(alpha) / 2 * 90.                               ## center of the octant
            decmin, decmax = 45. - delta_dec, 45. + delta_dec
            ramin, ramax = 45. - delta_ra, 45. + delta_ra
            mask_small_cat = (ra > ramin) & (ra < ramax) & (dec > decmin) & (dec < decmax)
            halocat_small_fname = halocat_slice_fname_template.format(nside_halo_subcatalogs, 0, name)
            halo_catalog_small = UserSuppliedHaloCatalog(redshift=redshift, Lbox=Lbox, particle_mass=particle_mass,
                                                         halo_x=xhalo[mask_small_cat],
                                                         halo_y=yhalo[mask_small_cat],
                                                         halo_z=zhalo[mask_small_cat],
                                                         halo_vx=vxhalo[mask_small_cat],
                                                         halo_vy=vyhalo[mask_small_cat],
                                                         halo_vz=vzhalo[mask_small_cat],
                                                         halo_id=ids[mask_small_cat],
                                                         halo_upid=-1*np.ones_like(ids)[mask_small_cat],
                                                         halo_hostid=ids[mask_small_cat],
                                                         halo_mvir=m[mask_small_cat],
                                                         halo_rvir=rvir[mask_small_cat])
            halo_catalog_small.add_halocat_to_cache(fname=halocat_small_fname,
                                                    simname=sim_name,
                                                    halo_finder=halo_finder,
                                                    version_name=version_name + "_npix_{}_{:04d}_{}".format(nside_halo_subcatalogs, pixel, name),
                                                    processing_notes="nside_halo_subcatalogs = {}".format(nside_halo_subcatalogs),
                                                    overwrite=True)
            del halo_catalog_small

    if DEBUG and rank==0:
        print("Done.")
        print("--------------")

    hdul.close()

    # Syncronizes everyone to prevent race conditions while master creates/load sliced catalogs
    if has_mpi_enabled:
        comm.barrier()

    # Loading photoz std and bias as function of redshift
    # (if including photoz uncertainties)
    do_add_photoz_uncertainties = options[module_section, "add_photoz_uncertainties"]
    if do_add_photoz_uncertainties:
        photoz_uncertainties_fname = options.get(module_section, "photoz_uncertainties_fname")
        photoz, photoz_std = np.loadtxt(photoz_uncertainties_fname, unpack=True)
        data["photoz"] = photoz
        data["photoz_std"] = photoz_std

    # Generate temp file name for mock galaxies
    temp_mock_fname = "mock_galaxies_rank_{}_realization_{}.npy".format(rank, "{}")

    # Writing configuration data to pass to execute
    data["fraction_on_zbin"] = fraction_on_zbin
    data["halo_masses"] = m
    data["num_ptcl_requirement"] = num_ptcl_requirement
    data["cosmology"] = cosmology
    data["mdef"] = mdef
    data["conc_mass_model"] = conc_mass_model
    data["nrealizations"] = nrealizations
    data["halocat_slice_fname_template"] = halocat_slice_fname_template
    data["nslices"] = nslices
    data["slice_pixels"] = occupied_pixels
    data["nside_halo_subcatalogs"] = nside_halo_subcatalogs
    data["zmin"] = zmin
    data["zmax"] = zmax
    data["estimated_area"] = estimated_area
    data["do_add_photoz_uncertainties"] = do_add_photoz_uncertainties
    data["max_memory"] = max_memory
    data["temp_mock_fname"] = temp_mock_fname

    return data


#@profile
def execute(block, config):
    fraction_on_zbin = config["fraction_on_zbin"]
    halo_masses = config["halo_masses"]
    num_ptcl_requirement = config["num_ptcl_requirement"]
    cosmology = config["cosmology"]
    mdef = config["mdef"]
    conc_mass_model = config["conc_mass_model"]
    nrealizations = config["nrealizations"]
    halocat_slice_fname_template = config["halocat_slice_fname_template"]
    nslices = config["nslices"]
    slice_pixels = config["slice_pixels"]
    nside_halo_subcatalogs = config["nside_halo_subcatalogs"]
    zmin = config["zmin"]
    zmax = config["zmax"]
    estimated_area = config["estimated_area"]
    do_add_photoz_uncertainties = config["do_add_photoz_uncertainties"]
    if do_add_photoz_uncertainties:
        photoz = config["photoz"]
        photoz_std = config["photoz_std"]
    max_memory = config["max_memory"]
    temp_mock_fname = config["temp_mock_fname"]

    hodpars = load_hod_dict(block)
    if DEBUG:
        print("---------------")
        print("HOD parameters:")
        for key in hodpars:
            print("{}: {:.10f}".format(key, hodpars[key]))
        print("---------------")

    ## Estimate memory consumption if the whole halo catalog is to be populated

    # Size, in bytes, each galaxy requires
    # Here estimate as the size of one row of halotools model.mock.galaxy_table
    bytes_per_galaxy = 112
    # Size, in bytes, each galaxy requires after processing
    # That is, the size of each row of the resulting array [ra, dec, z]
    bytes_per_galaxy_processed = 24
    ngals = estimate_ngals(hodpars, halo_masses, mdef, conc_mass_model)
    memory_in_gbytes = ngals * fraction_on_zbin * (nrealizations - 1) * bytes_per_galaxy_processed
    memory_in_gbytes += ngals * bytes_per_galaxy
    memory_in_gbytes *= 1.1            # Security factor
    memory_in_gbytes /= 1.e9

    # If projected memory is too big, try to use small (10 %) and smaller (1 %)
    # halo catalogs. If it is yet too big, stop here and write nan to datablock
    is_too_big = False
    if memory_in_gbytes < max_memory:
        name = "std"
        alpha = 1.
    else:
        if DEBUG:
            print("Projected memory consumption too big "
                  "({:.2f} Gb, vs. the maximum you indicated {:.2f} Gb) ".format(memory_in_gbytes, max_memory))

        slice_pixels = [0]
        if memory_in_gbytes * alpha_small < max_memory:
            if DEBUG:
                print("Using the small version of the halo catalog "
                      "with only {:.0f} % of the total area.".format(alpha_small))
            name = "small"
            estimated_area *= alpha_small
            alpha = alpha_small
        elif memory_in_gbytes * alpha_smaller < max_memory:
            if DEBUG:
                print("Using the smaller version of the halo catalog "
                      "with only {:.0f} % of the total area.".format(alpha_smaller))
            name = "smaller"
            estimated_area *= alpha_smaller
            alpha = alpha_smaller
        else:
            if DEBUG:
                print("Giving up of calculation and writing nans to the datablock.")
            is_too_big = True

    ## Produce mocks using one slice of the halo catalog at a time
    try:
        model = None
        ngals = np.zeros(nrealizations, dtype=int)
        gals = []
        if not is_too_big:
            for i, pixel in enumerate(slice_pixels):
                halocat_fname = halocat_slice_fname_template.format(nside_halo_subcatalogs, pixel, name)
                model = None
                halo_catalog = CachedHaloCatalog(fname=halocat_fname, update_cached_fname=True)
                gals_slice = []

                for j in range(nrealizations):
                    gals_slice_realization, model = populate_halos(hodpars, halo_catalog,
                                                                   cosmology=cosmology, model=model,
                                                                   mdef=mdef, conc_mass_model=conc_mass_model,
                                                                   num_ptcl_requirement=num_ptcl_requirement)
                    if DEBUG:
                        print("Pixel {}, realization {}: {} simulated galaxies".format(pixel, j,
                                                                                       len(gals_slice_realization)))

                    ## Add photoz uncertainties
                    if add_photoz_uncertainties:
                        gals_slice_realization[:,2] = add_photoz_uncertainties(gals_slice_realization[:,2], photoz, photoz_std, np.zeros_like(photoz_std))

                    ## Bin in redshift
                    gals_slice_realization_binned = eqinbin(gals_slice_realization, zmin, zmax)

                    ngals[j] += len(gals_slice_realization_binned)

                    gals_slice.append(gals_slice_realization_binned)

                    del gals_slice_realization_binned, gals_slice_realization
                gals.append(gals_slice)
                del halo_catalog

            ## Join in a single array all slices
            ## (can't do in single pass because we don't know in advance the number of galaxies)
            gals_joint = [np.zeros((ngals[i], 3)) for i in range(nrealizations)]

            for i in range(nrealizations):
                pivot = 0
                for gals_slice in gals:
                    gals_slice_realization = gals_slice[i]
                    slice_size = len(gals_slice_realization)
                    gals_joint[i][pivot:pivot+slice_size] = gals_slice_realization
                    pivot += slice_size

            ## Diagnostics
            if DEBUG:

                for i, realization in enumerate(gals_joint):
                    print("realization {}: simulated {} galaxies".format(i, len(realization)))

            ## Save mock galaxies to temporary file
            for i, gals_realization in enumerate(gals_joint):
                np.save(temp_mock_fname.format(i), gals_realization)

    except MemoryError:
        print("Memory error while generating mock catalog for HOD parameters:")
        for key in hodpars:
            print("{}: {:.10f}".format(key, hodpars[key]))
        is_too_big = True

    block[module_section, "halo_catalog_size"] = name
    block[module_section, "estimated_area"] = estimated_area
    block[module_section, "alpha"] = alpha
    block[module_section, "is_too_big"] = is_too_big
    block[module_section, "temp_mock_fname"] = temp_mock_fname
    block[module_section, "nrealizations"] = nrealizations

    return 0


def cleanup(config):
    temp_mock_fname = config["temp_mock_fname"]
    nrealizations = config["nrealizations"]
    for i in range(nrealizations):
        os.system("rm {}".format(temp_mock_fname.format(i)))
    del config
    return 0

