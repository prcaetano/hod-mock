import os
import numpy as np
import scipy.interpolate
from cosmosis.datablock import names
from cosmosis.gaussian_likelihood import GaussianLikelihood


class TwoPointLikelihood(GaussianLikelihood):
    like_name = "hod"
    x_section = names.data_vector
    x_name = "hod_x"
    y_section = names.data_vector
    y_name = "hod_y"

    y_cov_name = "hod_cov"
    constant_covariance = False
    loaded_data_covariance = False

    measured_gal_density_section = "sample_parameters"
    measured_gal_density_name = "density_of_galaxies"
    measured_gal_density_err_name = "density_of_galaxies_error"

    theory_gal_density_section = "hod_ngal"
    theory_gal_density_name = "gal_volume_density"
    theoy_gal_density_uncertainty = "gal_volume_density_err"

    sample_file_section = "sample_files"


    def build_data(self):
        self.block = self.options.block
        if self.block.has_value(self.sample_file_section, "acf"):
            self.acf_file = self.block.get_string(self.sample_file_section, "acf")
            self.data_x, self.data_y, self.data_y_err = np.loadtxt(self.acf_file, unpack=True)
        elif self.block.has_value(self.sample_file_section, "xi"):
            self.xi_file = self.block.get_string(self.sample_file_section, "xi")
            self.data_x, self.data_y, self.data_y_err = np.loadtxt(self.xi_file, unpack=True)
        else:
            raise RuntimeError("You should specify either the input acf or the input xi values under"
                               " the 'acf' or 'xi' option (resp.) on the 'sample_files' section of"
                               " your configuration file.")
        if self.options.get_bool("include_gal_density"):
            if self.block.has_value(self.measured_gal_density_section, self.measured_gal_density_name):
                gal_density = self.block[self.measured_gal_density_section, self.measured_gal_density_name]
                gal_density_err = self.block[self.measured_gal_density_section,
                                             self.measured_gal_density_err_name]

                new_data_x = np.zeros(len(self.data_x) + 1)
                new_data_y = gal_density * np.ones(len(self.data_x) + 1)
                new_data_y_err = gal_density_err * np.ones(len(self.data_x) + 1)
                new_data_x[1:] = self.data_x
                new_data_y[1:] = self.data_y
                new_data_y_err[1:] = self.data_y_err

                self.data_x = new_data_x
                self.data_y = new_data_y
                self.data_y_err = new_data_y_err
            else:
                raise RuntimeError("You should specify the measured galaxy density")
        return self.data_x, self.data_y


    def extract_theory_points(self, block):
        theory_x = block[self.x_section, self.x_name]
        theory_y = block[self.y_section, self.y_name]

        if self.options.get_bool("include_gal_density"):
            new_theory_x = np.zeros(len(theory_x) + 1)
            new_theory_y = np.zeros(len(theory_y) + 1)
            new_theory_x[0] = 0
            new_theory_y[0] = block[self.theory_gal_density_section, self.theory_gal_density_name]
            new_theory_x[1:] = theory_x
            new_theory_y[1:] = theory_y

            theory_x = new_theory_x
            theory_y = new_theory_y

        if self.options.get_bool("interpolate_xi", False):
            acf_interpolated_data = np.zeros_like(self.data_x)
            acf_interpolated_data[0] = theory_y[0]

            f = scipy.interpolate.interp1d(theory_x[1:], theory_y[1:], kind="cubic")
            try:
                acf_interpolated_data[1:] = np.atleast_1d(f(self.data_x[1:]))
            except ValueError:
                raise RuntimeError("Some datavector x values fell outside the range of "
                                   "theoretical calculation.")
        else:
            if not np.isclose(theory_x, self.data_x, atol=1e-3).all():
                print("WARNING: You set interpolate_xi to false for the likelihood computation "
                      "but the nominal centers of the bins used to compute the correlation function "
                      "for the mock catalogs seem to disagree with the values loaded from the data, "
                      "what might indicate that data and mock correlation functions were calculated "
                      "with different binnings. Be sure they are compatible or enable interpolation.")
                print("Theory: ", theory_x)
                print("Data: ", self.data_x)
            acf_interpolated_data = theory_y

        return acf_interpolated_data


    def extract_covariance(self, block):

        if self.block.has_value(self.sample_file_section, "cov") and not self.loaded_data_covariance:
            self.cov_file = self.block.get_string(self.sample_file_section, "cov")
            if os.path.exists(self.cov_file):
                cov_flat = np.loadtxt(self.cov_file)
                try:
                    nx = self.data_x.size - int(self.options.get_bool("include_gal_density"))
                    self.data_covariance = cov_flat.reshape(nx, nx)
                except ValueError:
                    raise RuntimeError("Incompatible sizes for covariance and data given.")

                if self.options.get_bool("include_gal_density"):
                    new_data_covariance = np.zeros((nx+1, nx+1))
                    new_data_covariance[0,0] = self.data_y_err[0]**2
                    new_data_covariance[1:,1:] = self.data_covariance

                    self.data_covariance = new_data_covariance

        elif not self.loaded_data_covariance:
            self.data_covariance = np.diag(self.data_y_err**2)

        self.loaded_data_covariance = True

        self.cov = self.data_covariance
        if self.options.get_bool("add_theoretical_covariance", False):
            if block.has_value(self.y_section, self.y_cov_name):
                theoretical_cov = block[self.y_section, self.y_cov_name]
                if self.options.get_bool("include_gal_density"):
                    n = theoretical_cov.shape[0]
                    new_theoretical_cov = np.zeros((n+1, n+1))
                    new_theoretical_cov[0, 0] = block[self.theory_gal_density_section,
                                                      self.theoy_gal_density_uncertainty]**2
                    new_theoretical_cov[1:, 1:] = theoretical_cov
                    theoretical_cov = new_theoretical_cov
                self.cov += theoretical_cov

        return self.cov


    def extract_inverse_covariance(self, block):
        inv_cov = np.zeros_like(self.cov)
        if self.options.get_bool("include_gal_density"):
            inv_cov[1:,1:] = np.linalg.inv(self.cov[1:,1:])
            inv_cov[0,0] = 1. / self.cov[0,0]
        else:
            inv_cov = np.linalg.inv(self.cov)
        return inv_cov


setup, execute, cleanup = TwoPointLikelihood.build_module()

