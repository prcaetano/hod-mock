""" nn.py --- position-position
"""
from treecorr import Catalog, NNCorrelation
from fitsio import read_header
from sys import argv

sep_units="degrees"
DEBUG = True

def wtheta(lcat, rcat, rrf, ofn, min_sep, max_sep, nbins, bs):
    dd = NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins, sep_units=sep_units, bin_slop=bs)
    dr = NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins, sep_units=sep_units, bin_slop=bs)
    rr = NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins, sep_units=sep_units, bin_slop=bs)
    Ncat = Catalog(lcat, ra_col='RA', dec_col='DEC', ra_units='deg', dec_units='deg')
    dr.process(Ncat, cat2=Catalog(rcat, ra_col='RA', dec_col='DEC', ra_units='deg', dec_units='deg'))
    dd.process(Ncat)
    rr.read(rrf); rr.tot=read_header(rrf, 1)['TOT']
    dd.write(ofn, rr=rr, dr=dr)


if __name__ == '__main__':
    if DEBUG:
        print("Entering nn.py.")
        print("Call: {}".format(argv[1:]))

    lcat, rcat, rrf, ofn, min_sep, max_sep, nbins, bs = argv[1:]
    min_sep = float(min_sep)
    max_sep = float(max_sep)
    nbins = int(nbins)
    bs = float(bs)
    wtheta(lcat, rcat, rrf, ofn, min_sep, max_sep, nbins, bs)

