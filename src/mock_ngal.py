import numpy as np
from astropy.cosmology import FlatLambdaCDM
#from memory_profiler import profile

#DEBUG = False
DEBUG = True

sim_parameters = "simulation_parameters"
hod_parameters = "hod_parameters"
module_section = "hod_ngal"
mock_module_section = "hod_mock"


def calculate_galaxy_volume_density(ngals, volume):
    """
    Computes volume density of galaxies.

    Parameters:
        ngals: np.ndarray
            array holding counts of mock galaxies obtained over various realizations
        volume: float
            volume, in Mpc^3, of the simulated region

    Returns:
        gal_volume_density, gal_volume_density_err

        gal_volume_density: float
            mean galaxy densities over realizations, in gal/Mpc^3,
            for each redshift bin
        gal_volume_density_err: float
            galaxy density uncertainties (calculated as std over the
            realizations) for each redshift bin
    """
    gal_volume_density_realizations = []
    gal_volume_density_realizations = np.array(ngals)
    gal_volume_density_realizations = gal_volume_density_realizations / volume

    gal_volume_density = gal_volume_density_realizations.mean()
    gal_volume_density_err = gal_volume_density_realizations.std()

    if len(gal_volume_density_realizations) <= 1:
        gal_volume_density_err = np.nan

    return gal_volume_density, gal_volume_density_err


def load_hod_dict(block):
    """ Loads hod parameters as dictionary from datablock. """
    hod_keys = ["sigmam", "massmin", "alpha", "mass0", "mass1", "fcentral"]
    hodpars = {key: block[hod_parameters, key] for key in hod_keys}
    return hodpars


def setup(options):
    block = options
    data = dict()

    # Check if mpi is using to prevent race conditions later on
    try:
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()
        if size > 1:
            has_mpi_enabled = True
        else:
            has_mpi_enabled = False
    except ImportError:
        comm = None
        rank = 0
        size = 1
        has_mpi_enabled = False

    # Loading simulation configurations
    omegam = block.get_double(sim_parameters, "omegam")
    omegab = block.get_double(sim_parameters, "omegab")
    h = block.get_double(sim_parameters, "h")

    # Loading module configurations
    zmin = options.get(mock_module_section, "zmin")
    zmax = options.get(mock_module_section, "zmax")

    # Loading simulation data
    cosmology = FlatLambdaCDM(Om0=omegam, Ob0=omegab, H0=100*h, Tcmb0=2.73)

    # Writing configuration data to pass to execute
    data["cosmology"] = cosmology
    data["zmin"] = zmin
    data["zmax"] = zmax

    return data


#@profile
def execute(block, config):
    cosmology = config["cosmology"]
    zmin = config["zmin"]
    zmax = config["zmax"]

    name = block[mock_module_section, "halo_catalog_size"]
    estimated_area = block[mock_module_section, "estimated_area"]
    alpha = block[mock_module_section, "alpha"]
    is_too_big = block[mock_module_section, "is_too_big"]
    temp_mock_fname = block[mock_module_section, "temp_mock_fname"]
    nrealizations = block[mock_module_section, "nrealizations"]
    gals_joint = [np.load(temp_mock_fname.format(i)) for i in range(nrealizations)]

    hodpars = load_hod_dict(block)

    if is_too_big:
        block[module_section, "gal_volume_density"] = np.nan
        block[module_section, "gal_volume_density_err"] = np.nan
    else:
        # Compute number of galaxies in each realization
        ngals = np.array([len(realization) for realization in gals_joint])

        # Calculate volume density and write to datablock
        frac_sky = estimated_area / (4*np.pi * (180./np.pi)**2)
        volume = (cosmology.comoving_volume(zmax) - cosmology.comoving_volume(zmin)).value * frac_sky
        gal_volume_density, gal_volume_density_err = calculate_galaxy_volume_density(ngals, volume)
        block[module_section, "gal_volume_density"] = gal_volume_density
        block[module_section, "gal_volume_density_err"] = gal_volume_density_err

    return 0


def cleanup(config):
    del config
    return 0

