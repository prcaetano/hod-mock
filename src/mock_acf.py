import os
import sys
import numpy as np
import healpy as hp
import treecorr as tc
from astropy.table import Table
from cosmosis.datablock import names
from cosmosis.datablock import BlockError
from subprocess import Popen

#from memory_profiler import profile

#DEBUG = False
DEBUG = True

sim_parameters = "simulation_parameters"
hod_parameters = "hod_parameters"
module_section = "hod_acf"
mock_module_section = "hod_mock"

alphas = {"std": 1, "small": 0.1, "smaller": 0.01}
theta_maxs = {name: 2 * min(45. - 1./2 * np.arcsin(1 - alphas[name]) * 180. / np.pi,
                            np.sqrt(alphas[name]) / 2 * 90.)
              for name in alphas.keys()}


def calculate_galaxy_areal_density(ngals, area=None):
    """
    Computes areal density of galaxies. This function assumes that the total simulated
    area is that of a full octant of the sky.

    Parameters:
        ngals: np.ndarray
            array holding counts of mock galaxies obtained over various realizations
        area: float
            area of observed region, in sq. deg. Assumed to be one octant of sphere if not given

    Returns:
        gal_area_density, gal_area_density_err

        gal_area_density: float
            mean galaxy densities over realizations, in gal/sq. deg.,
            for each redshift bin
        gal_area_density_err: float
            galaxy density uncertainties (calculated as std over the
            realizations) for each redshift bin
    """
    octant_area = np.pi/2./(np.pi/180)**2
    if area is None:
        area = octant_area

    gal_area_density_realizations = []
    gal_area_density_realizations = np.array(ngals)
    gal_area_density_realizations = gal_area_density_realizations / area

    gal_area_density = gal_area_density_realizations.mean()
    gal_area_density_err = gal_area_density_realizations.std()

    return gal_area_density, gal_area_density_err


#@profile
def calculate_wtheta(gals_joint_photoz, mock_cat_fname, random_cat_fname_template,
                     random_precomputed_fname_template, corrfunc_fname,
                     min_sep, max_sep, nbins, binslop):
    """
    Compute wtheta for mock galaxies gals_joint_photoz.


    Parameters:
        gals_joint_photoz: list of np.ndarray
            list containing the different realizations of mock galaxies. Each realization
            is a numpy array with galaxies whose rows contains the mock galaxies, with the
            columns holding the ra, dec an photoz.
        mock_cat_fname: str
            path to temporary fits file that will be created holding the mock galaxies for
            treecorr external computation
        random_cat_fname_template: str
            template path to temporary fits file holding the random galaxies for treecorr external
            computation, with placeholder to be filled in with the realization index, so that
            different realizations don't share the same randoms (this should already be created)
        random_precomputed_fname_template: str
            template path to temporary file holding the precomputed random-random pairs,
            with placeholder to be filled with realization index
        corrfunc_fname: str
            path to temporary file that will hold the computed correlation function
        min_sep: float
            minimum separation to use when binning galaxy pairs, in degrees
        max_sep: float
            maximum separation to use when binning galaxy pairs, in degrees
        nbins: int
            number of bins
        binslop: float
            treecorr binslop parameter

    Returns:
        theta, w, var_w

        theta: np.ndarray
            mean values of theta bins
        w: np.ndarray
            array containing the measured wtheta for all the given realizations
        var_w: np.ndarray
            array containing the poisson noise on the measurements in w
    """
    w_realizations = []
    varw_realizations = []

    for i, gals_realization_binned in enumerate(gals_joint_photoz):

        random_cat_fname = random_cat_fname_template.format(i)
        random_precomputed_fname = random_precomputed_fname_template.format(i)

        if len(gals_realization_binned) == 0:
            ret = np.array([])
            return ret, ret, ret

        # Create temporary file holding the mock data
        mock_temp_table = Table([gals_realization_binned[:,0], gals_realization_binned[:,1]],
                                names=("RA", "DEC"))
        mock_temp_table.write(mock_cat_fname, overwrite=True)

        # Calling treecorr convenience scripts to compute the correlation function
        env = dict(os.environ)
        cmd = "python"
        script = os.path.dirname(os.path.realpath(__file__)) + "/nn.py"
        args = [cmd, script, mock_cat_fname, random_cat_fname, random_precomputed_fname,
                corrfunc_fname, min_sep, max_sep, nbins, binslop]
        args = [str(arg) for arg in args]
        return_code = Popen(args, env=env).wait()
        if return_code == -9:
            raise MemoryError

        # Read computed correlation function
        corrfunc_table = Table.read(corrfunc_fname)
        w = corrfunc_table["xi"]
        var_w = (corrfunc_table["sigma_xi"]**2)
        theta = corrfunc_table["meanr"]

        w_realizations.append(w)
        varw_realizations.append(var_w)

    w = np.array(w_realizations)
    var_w = np.array(varw_realizations)

    return theta, w, var_w


def produce_preprocess_randoms(nrandoms, random_cat_fname_templates, random_precomputed_fname_templates,
                               mpi_size, min_sep, max_sep, nbins, binslop):
    """
    Produces and preprocess randoms by calling external treecorr script.


    Parameters:
        nrandoms: int
            size of random catalog to generate
        random_cat_fname_templates: dict of str
            dict with template path to temporary fits files that will be created holding the random
            galaxies for treecorr external computation, indexed by name of catalog (that is,
            "std", "small" or "smaller"), containing space for rank (of mpi process) to be filled in
        random_precomputed_fname_templates: dict of str
            template path to temporary file holding the precomputed random-random pairs, indexed by
            name of catalog ("std", "small" or "smaller") and contained space for rank to be filled in
        mpi_size: int
            number of mpi processes
            (also the number of copies of the random precomputed pairs file to be created)
        min_sep: float
            minimum separation to use when binning galaxy pairs, in degrees
        max_sep: float
            maximum separation to use when binning galaxy pairs, in degrees
        nbins: int
            number of bins
        binslop: float
            treecorr binslop parameter


    Returns:
        nothing
    """
    # Producing random catalog for acf calculation
    random_dec = np.arcsin(np.random.rand(nrandoms)) * 180 / np.pi
    random_ra = np.random.rand(nrandoms) * 90

    random_cat_fnames = {name: random_cat_fname_templates[name].format(0)
                         for name in random_cat_fname_templates}
    random_precomputed_fnames = {name: random_precomputed_fname_templates[name].format(0)
                                 for name in random_precomputed_fname_templates}

    # Preprocessing random-random pair computations
    for name in alphas.keys():

        # Skipping if already computed
        if os.path.exists(random_cat_fnames[name]) and os.path.exists(random_precomputed_fnames[name]):
            continue

        alpha = alphas[name]
        delta_dec = 45. - 1./2 * np.arcsin(1 - alpha) * 180. / np.pi    ## Cuts 1./ alpha of area around
        delta_ra = np.sqrt(alpha) / 2 * 90.                             ## center of the octant
        decmin, decmax = 45. - delta_dec, 45. + delta_dec
        ramin, ramax = 45. - delta_ra, 45. + delta_ra
        mask_small_cat = (random_ra > ramin) & (random_ra < ramax) & (random_dec > decmin) & (random_dec < decmax)
        # Create temporary file holding randoms
        random_temp_table = Table([random_ra[mask_small_cat], random_dec[mask_small_cat]],
                                  names=("RA", "DEC"))
        random_temp_table.write(random_cat_fnames[name], overwrite=True)

        # Calling treecorr external script for random precomputation
        env = dict(os.environ)
        cmd = "python"
        script = os.path.dirname(os.path.realpath(__file__)) + "/rr.py"
        args = [cmd, script, random_cat_fnames[name], random_precomputed_fnames[name],
                min_sep, max_sep, nbins, binslop]
        args = [str(arg) for arg in args]
        Popen(args, env=env).wait()

    # Making copies for each mpi process to access independently
    for i in range(1, mpi_size):
        for name in alphas.keys():
            random_cat_fname_cp = random_cat_fname_templates[name].format(i)
            random_precomputed_fname_cp = random_precomputed_fname_templates[name].format(i)

            if os.path.exists(random_cat_fname_cp) and os.path.exists(random_precomputed_fname_cp):
                continue

            env = dict(os.environ)
            cmd = "cp"
            args = [cmd, random_cat_fnames[name],
                   random_cat_fname_templates[name].format(i)]
            Popen(args, env=env).wait()

            args = [cmd, random_precomputed_fnames[name],
                    random_precomputed_fname_templates[name].format(i)]
            Popen(args, env=env).wait()


def setup(options):
    block = options
    data = dict()

    # Horrible hack to skip this when retraining (training but with mocks already produced)
    try:
        if options.get(module_section, "skip_setup"):
            print("Skipping setup on mock_acf because you told me so...")
            return data
    except BlockError:
        pass

    # Check if mpi is used and get the corresponding rank / size of the pool
    try:
        from mpi4py import MPI
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()
        if size > 1:
            has_mpi_enabled = True
        else:
            has_mpi_enabled = False
    except ImportError:
        comm = None
        rank = 0
        size = 1
        has_mpi_enabled = False

    # Loading module configurations
    nrandoms = options.get(module_section, "nrandoms")
    min_sep = options.get(module_section, "min_sep")
    max_sep = options.get(module_section, "max_sep")
    nbins = options.get(module_section, "nbins")
    bin_slop = options.get(module_section, "bin_slop")
    include_gal_density = options.get(module_section, "include_gal_density")
    try:
        max_memory = options.get(module_section, "max_memory")
    except BlockError:
        max_memory = np.inf
    nrealizations = options.get(mock_module_section, "nrealizations")

    mock_cat_fname = "mock_galaxies_{}.fits".format(rank)
    random_cat_fnames = {name: "random_galaxies_{}_{}_{}.fits".format(name, "{}", "{}")
                         for name in ["std", "small", "smaller"]}
    random_precomputed_fnames = {name: "random_precomputed_{}_{}_{}.fits".format(name, "{}", "{}")
                                 for name in ["std", "small", "smaller"]}
    corrfunc_fname = "corrfunc_{}.fits".format(rank)

    # Produce randoms and call external script to precomputed rr pairs
    for i in range(rank, nrealizations, size):
        random_cat_fname_templates = {name: random_cat_fnames[name].format("{}", i)
                                      for name in random_cat_fnames}
        random_precomputed_fname_templates = {name: random_precomputed_fnames[name].format("{}", i)
                                              for name in random_precomputed_fnames}

        produce_preprocess_randoms(nrandoms, random_cat_fname_templates,
                                   random_precomputed_fname_templates, size,
                                   min_sep, max_sep, nbins, bin_slop)

    random_cat_fnames = {name: random_cat_fnames[name].format(rank, "{}")
                         for name in random_cat_fnames}
    random_precomputed_fnames = {name: random_precomputed_fnames[name].format(rank, "{}")
                                 for name in random_precomputed_fnames}

    # Writing configuration data to pass to execute
    data["mock_cat_fname"] = mock_cat_fname
    data["random_cat_fnames"] = random_cat_fnames
    data["random_precomputed_fnames"] = random_precomputed_fnames
    data["corrfunc_fname"] = corrfunc_fname
    data["include_gal_density"] = include_gal_density
    data["min_sep"] = min_sep
    data["max_sep"] = max_sep
    data["nbins"] = nbins
    data["bin_slop"] = bin_slop
    data["nrealizations"] = nrealizations

    # MPI sync
    if has_mpi_enabled:
        comm.barrier()

    return data


#@profile
def execute(block, config):
    mock_cat_fname = config["mock_cat_fname"]
    random_cat_fnames = config["random_cat_fnames"]
    random_precomputed_fnames = config["random_precomputed_fnames"]
    corrfunc_fname = config["corrfunc_fname"]
    include_gal_density = config["include_gal_density"]
    min_sep = config["min_sep"]
    max_sep = config["max_sep"]
    nbins = config["nbins"]
    bin_slop = config["bin_slop"]
    nrealizations = config["nrealizations"]

    name = block[mock_module_section, "halo_catalog_size"]
    estimated_area = block[mock_module_section, "estimated_area"]
    alpha = block[mock_module_section, "alpha"]
    is_too_big = block[mock_module_section, "is_too_big"]
    temp_mock_fname = block[mock_module_section, "temp_mock_fname"]
    if not is_too_big:
        gals_joint = [np.load(temp_mock_fname.format(i)) for i in range(nrealizations)]
        ngals = np.array([len(realization) for realization in gals_joint])

    random_cat_fname = random_cat_fnames[name]
    random_precomputed_fname = random_precomputed_fnames[name]

    if is_too_big:
        # Write nan to all values that should be calculated
        gal_area_density = np.nan
        gal_area_density_err = np.nan

        theta = np.logspace(min_sep, max_sep, nbins)
        w_mean = np.nan * theta
        w_cov = w_mean.reshape(1, -1) * w_mean.reshape(-1, 1)
        w_var = np.diag(w_cov)
        w_std = np.sqrt(w_var)

    else:
        try:
            # Calculate area density and write to datablock
            gal_area_density, gal_area_density_err = calculate_galaxy_areal_density(ngals, estimated_area)
            block[module_section, "gal_area_density"] = gal_area_density
            block[module_section, "gal_area_density_err"] = gal_area_density_err

            # Calculate acf and write to datablock
            theta, w, w_std_poisson = calculate_wtheta(gals_joint, mock_cat_fname, random_cat_fname,
                                                       random_precomputed_fname, corrfunc_fname,
                                                       min_sep, max_sep, nbins, bin_slop)

            if len(theta) == 0:
                print("ERROR: no galaxies in given redshift limits!", file=sys.stderr)
                return 1

            # Compute means/variances and mask invalid values
            w_mean = np.ma.masked_invalid(w).mean(axis=0).filled(np.nan)
            if nrealizations > 1:
                w_cov = np.ma.cov(np.ma.masked_invalid(w.T)).filled(np.nan)
                w_var = np.diag(w_cov)
                w_std = np.sqrt(w_var)

            ## Mask angles too big for the simulated catalog size
            if theta.max() > theta_maxs[name]:
                print("WARNING: Maximum value of theta in acf calculation is bigger then "
                      "the maximum angle in the simulated catalog. Setting the acf for "
                      "those values to nan.")
            mask_flat = theta > theta_maxs[name]

            w_mean[mask_flat] = np.nan
            if nrealizations < 1:
                mask_cov = mask_flat.reshape(1, -1) | mask_flat.reshape(-1, 1)
                w_var = w_var[mask_flat]
                w_std = w_std[mask_flat]
                w_cov[mask_cov] = np.nan
        except MemoryError:
            print("Memory error while computing ACF of mock catalog for HOD parameters:")
            hod_keys = ["sigmam", "massmin", "alpha", "mass0", "mass1", "fcentral"]
            hodpars = {key: block[hod_parameters, key] for key in hod_keys}
            for key in hodpars:
                print("{}: {:.10f}".format(key, hodpars[key]))
            print("Writing nans to datablock")
            gal_area_density = np.nan
            gal_area_density_err = np.nan

            theta = np.logspace(min_sep, max_sep, nbins)
            w_mean = np.nan * theta
            w_cov = w_mean.reshape(1, -1) * w_mean.reshape(-1, 1)
            w_var = np.diag(w_cov)
            w_std = np.sqrt(w_var)

    # Write stuff to datablock
    block[module_section, "hod_theta"] = theta
    block[module_section, "hod_acf"] = w_mean
    if nrealizations > 1:
        block[module_section, "hod_acf_err"] = w_std
        block[module_section, "hod_acf_cov"] = w_cov

    if include_gal_density:
        n_data_vector = len(theta) + 1
        data_vector_x = np.zeros(n_data_vector)
        data_vector_x[1:] = theta
        data_vector_y = gal_area_density * np.ones_like(data_vector_x)
        data_vector_y[1:] = w_mean
        if nrealizations > 1:
            data_vector_cov = np.zeros((n_data_vector, n_data_vector))
            data_vector_cov[0,0] = gal_area_density_err**2
            data_vector_cov[1:,1:] = w_cov
    else:
        data_vector_x = theta
        data_vector_y = w_mean
        if nrealizations > 1:
            data_vector_cov = w_cov

    block[names.data_vector, "hod_x"] = data_vector_x
    block[names.data_vector, "hod_y"] = data_vector_y
    if nrealizations > 1:
        block[names.data_vector, "hod_y_err"] = np.diagonal(data_vector_cov)
        block[names.data_vector, "hod_cov"] = data_vector_cov

    return 0


def cleanup(config):
    if len(config) == 0:
        return 0

    mock_cat_fname = config["mock_cat_fname"]
    random_cat_fnames = config["random_cat_fnames"]
    random_precomputed_fnames = config["random_precomputed_fnames"]
    corrfunc_fname = config["corrfunc_fname"]
    nrealizations = config["nrealizations"]

    rm_cmd_template = "rm -f {}"
    os.system(rm_cmd_template.format(mock_cat_fname))
    for name in random_cat_fnames.keys():
        for i in range(nrealizations):
            random_cat_fname = random_cat_fnames[name].format(i)
            random_precomputed_fname = random_precomputed_fnames[name].format(i)
            os.system(rm_cmd_template.format(random_cat_fname))
            os.system(rm_cmd_template.format(random_precomputed_fname))
    os.system(rm_cmd_template.format(corrfunc_fname))

    del config
    return 0

