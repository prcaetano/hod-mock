import os
import numpy as np
import pandas as pd
from astropy.io import fits
from astropy.cosmology import FlatLambdaCDM, z_at_value
from astropy.constants import c
from astropy import units as u
from halotools.empirical_models import PrebuiltHodModelFactory
from scipy.stats import norm
from scipy.interpolate import interp1d
from mock_generation import load_hod_dict


sim_parameters = "simulation_parameters"
hod_parameters = "hod_parameters"
module_section = "hod_ngal"


def compute_ngal(hodpar, nhalos, mass):
    """
    Computes galaxy volume density given hod parameters and halo mass function.

    Parameters:
        hodpar: dict
            hod parameters to compute the galaxy volume density
        nhalos: np.ndarray
            halo volume density (units: 1./Mpc**3) for mass bin whose
            centers are given in the mass array
        mass: np.ndarray
            centers of bins used to compute the halo volume density

    Returns:
        ngals

        ngals: float
            galaxy volume density
    """

    model = PrebuiltHodModelFactory('zheng07', modulate_with_cenocc=False,
                                    conc_mass_model="dutton_maccio14", mdef="vir")
    model.param_dict['alpha'] = hodpar['alpha']
    model.param_dict['logM0'] = hodpar['mass0']
    model.param_dict['logM1'] = hodpar['mass1']
    model.param_dict['logMmin'] = hodpar['massmin']
    model.param_dict['sigma_logM'] = hodpar['sigmam']

    mean_n = model.mean_occupation_centrals(prim_haloprop=mass) \
            * (hodpar["fcentral"] + model.mean_occupation_satellites(prim_haloprop=mass))
    ngals = (nhalos * mean_n).sum()

    return ngals


def setup(options):
    block = options
    data = dict()

    # Loading simulation configurations
    Lbox = block.get_double(sim_parameters, "Lbox")
    particle_mass = block.get_double(sim_parameters, "particle_mass")
    omegam = block.get_double(sim_parameters, "omegam")
    omegab = block.get_double(sim_parameters, "omegab")
    h = block.get_double(sim_parameters, "h")
    num_ptcl_requirement = block.get_int(sim_parameters, "halo_min_num_particles")
    catalog_fname = block.get_string(sim_parameters, "halo_catalog_fname")
    estimated_area = block.get_double(sim_parameters, "area")

    # Loading module configurations
    zmin = options.get(module_section, "zmin")
    zmax = options.get(module_section, "zmax")
    photoz_uncertainties_fname = options.get(module_section, "photoz_uncertainties_fname")

    # Loading halo data
    cosmology = FlatLambdaCDM(Om0=omegam, Ob0=omegab, H0=100*h, Tcmb0=2.73)
    # Computing volume of observed region
    frac_sky = estimated_area / (4*np.pi * (180./np.pi)**2)
    volume = frac_sky * (cosmology.comoving_volume(zmax) - cosmology.comoving_volume(zmin)).value

    if os.path.exists("halo_mass_function.npy"):
        mass, Nhalos = np.load("halo_mass_function.npy")
    else:
        hdul = fits.open(catalog_fname, memmap=True)

        logm = hdul[1].data["lmhalo"]
        z = hdul[1].data["z_cgal"]

        max_z = z_at_value(lambda x: cosmology.comoving_distance(x).value * h, Lbox)
        mask_max_z = z < max_z
        mask_min_mass = logm > np.log10(num_ptcl_requirement * particle_mass)
        mask_halos = mask_max_z & mask_min_mass
        logm = logm[mask_halos]

        xhalo = hdul[1].data["xhalo"][mask_halos]
        yhalo = hdul[1].data["yhalo"][mask_halos]
        zhalo = hdul[1].data["zhalo"][mask_halos]

        vxhalo = hdul[1].data["vxhalo"][mask_halos]
        vyhalo = hdul[1].data["vyhalo"][mask_halos]
        vzhalo = hdul[1].data["vzhalo"][mask_halos]

        halo_pos = np.c_[xhalo, yhalo, zhalo]
        halo_vel = np.c_[vxhalo, vyhalo, vzhalo]

        comoving_distances = np.linalg.norm(halo_pos, axis=1)
        los_velocity = (halo_vel*halo_pos).sum(axis=1) / comoving_distances

        c_km_s = c.to('km/s').value
        h = cosmology.H0.to(u.km / u.Mpc / u.s).value / 100

        zgrid = np.linspace(0., 2., 200000)
        comoving_distances_grid = cosmology.comoving_distance(zgrid).value * h

        z_cos = np.interp(comoving_distances, comoving_distances_grid, zgrid)

        # Adding RSD to halos
        z = z_cos + los_velocity / c_km_s * (1. + z_cos)

        # Adding photoz uncertainties
        photoz, photoz_std = np.loadtxt(photoz_uncertainties_fname, unpack=True)

        std_z = interp1d(photoz, photoz_std)(z)
        p_bin = norm.cdf(zmax, loc=z, scale=std_z) - norm.cdf(zmin, loc=z, scale=std_z)
        ## p_bin is the probability each galaxy in the halo has of ending up in
        ## the shell delimited by zmin and zmax

        m = 10**logm
        mass_bin_edges = np.logspace(logm.min(), logm.max(), num=1000)

        df = pd.DataFrame({"p_bin": p_bin, "m": m})
        df_groupby = df.groupby(pd.cut(df["m"], bins=mass_bin_edges))
        Nhalos = np.array(df_groupby["p_bin"].sum())
        mass = np.array(df_groupby["m"].mean())

        mask_nan = ~np.isnan(mass)
        Nhalos = Nhalos[mask_nan]
        mass = mass[mask_nan]

        np.save("halo_mass_function.npy", np.array([mass, Nhalos]))

    # Computing halo mass function
    nhalos = Nhalos / volume

    data["nhalos"] = nhalos
    data["mass"] = mass
    data["volume"] = volume

    return data


def execute(block, config):
    nhalos = config["nhalos"]
    mass = config["mass"]
    volume = config["volume"]

    hodpars = load_hod_dict(block)
    ngals = compute_ngal(hodpars, nhalos, mass)

    # Approximating uncertainty by Poisson error
    ngals_err = 1. / np.sqrt(ngals * volume) * ngals

    block["hod_ngal", "gal_volume_density"] = ngals
    block["hod_ngal", "gal_volume_density_err"] = ngals_err

    return 0


def cleanup(config):
    return 0

