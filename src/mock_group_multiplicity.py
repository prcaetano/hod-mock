import numpy as np
from astropy.cosmology import FlatLambdaCDM
from halotools.mock_observables import FoFGroups

module_section = "hod_group_multiplicity"
mock_module_section = "hod_mock"

b_perp = 0.7
b_para = 0.15


def get_positions_from_radecz(ra, dec, redshift, cosmology):
        """
        TODO: Document this.
        """
        h = cosmology.h
        comoving_distance = cosmology.comoving_distance(redshift).value * h  # units: Mpc/h
        phi = ra * np.pi / 180.
        theta = (np.pi/2. - dec * np.pi / 180.)
        z = comoving_distance * np.cos(theta)
        xy = comoving_distance * np.sin(theta)
        x = xy * np.cos(phi)
        y = xy * np.sin(phi)
        return np.c_[x, y, z]


def get_group_multiplicity(gals, bins, Lbox, volume, cosmology):
    """
    Compute group multiplicity function (density of fof groups as a function of number of
    galaxies in group).

    Parameters:
        gals: list of np.ndarray
            array holding the different realizations of the galaxy population. Each
            realization is an array with shape (ngals, 3), whose columns contain
            the ra, dec and redshift, resp.
        bins: np.ndarray
            values of number of galaxies to bin at (bins are left open, right closed)
        Lbox: float
            value of the simulation box side, in Mpc/h
        volume: float
            volume of simulated region, in Mpc/h**3
        cosmology: astropy cosmology object
            self-explanatory

    Returns:
        group_multiplicity, group_multiplicity_err

        group_multiplicity: np.ndarray
            number density of groups, as a function of number of galaxies in group
        group_mulitiplicity_err: np.ndarray
            standard deviation of group_multiplicity over the realizations
    """
    group_multiplicities = []
    for realization in gals:
        gal_pos = get_positions_from_radecz(gals[:,0], gals[:,1], gals[:,1], cosmology)
        groups = FoFGroups(gal_pos, b_perp, b_para,
                           Lbox=Lbox, num_threads='max', volume=volume)
        fof_group_ids = groups.group_ids
        ids, counts = np.unique(fof_group_ids, return_counts=True)
        group_multiplicity = np.histogram(counts, bins=bins)[0] / volume
        group_multiplicities.append(group_multiplicity)

    group_multiplicity = group_multiplicities.mean()
    group_multiplicity_err = group_multiplicities.std()

    if len(gals) <= 1:
        group_multiplicity_err = np.nan

    return group_multiplicity, group_multiplicity_err


def setup(options):
    block = options
    data = dict()

    ## TODO: Fill with loading of relevant configuration

    return data

def execute(block, config):
    cosmology = config["cosmology"]
    zmin = config["zmin"]
    zmax = config["zmax"]
    Lbox = config["Lbox"]
    bins = config["bins"]

    name = block[mock_module_section, "halo_catalog_size"]
    estimated_area = block[mock_module_section, "estimated_area"]
    alpha = block[mock_module_section, "alpha"]
    is_too_big = block[mock_module_section, "is_too_big"]
    temp_mock_fname = block[mock_module_section, "temp_mock_fname"]
    nrealizations = block[mock_module_section, "nrealizations"]
    gals_joint = [np.load(temp_mock_fname.format(i)) for i in range(nrealizations)]

    h = cosmology.h
    volume = (cosmology.comoving_volume(zmax).value - cosmology.comoving_volume(zmin).value) * h**3
    volume *= estimated_area / (4*np.pi * (180. / np.pi)**2)

    group_multiplicity, group_multiplicity_err = get_group_multiplicity(gals_joint, bins, Lbox, volume)

    block[module_section, "group_multiplicity_bins"] = group_multiplicity_bins
    block[module_section, "group_multiplicity"] = group_multiplicity
    block[module_section, "group_multiplicity_err"] = group_multiplicity_err

    return 0

def cleanup(config):
    del config
    return 0

