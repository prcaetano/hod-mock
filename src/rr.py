""" rr.py --- RR computation alone
"""
from treecorr import NNCorrelation, Catalog
from fitsio import FITS
from sys import argv

DEBUG = True

def rr(ifn, ofn, min_sep, max_sep, nbins, bs):
    rr = NNCorrelation(min_sep=min_sep, max_sep=max_sep, nbins=nbins,
                       sep_units='degrees', bin_slop=bs)
    rr.process(Catalog(ifn, ra_col='RA', dec_col='DEC', ra_units='deg', dec_units='deg'))
    rr.write(ofn)
    f = FITS(ofn, 'rw') 
    f[1].write_key('TOT', rr.tot, comment="Total number of pairs processed")
    f.close()


if __name__ == '__main__':
    if DEBUG:
        print("Entering rr.py.")
        print("Call: {}".format(argv[1:]))

    ifn, ofn, min_sep, max_sep, nbins, bs = argv[1:]
    min_sep = float(min_sep)
    max_sep = float(max_sep)
    nbins = int(nbins)
    bs = float(bs)
    rr(ifn, ofn, min_sep, max_sep, nbins, bs)

